---
Structure: 03
Content: Three for tea
Initials: NISI
---

# Three for tea

1. Teammember 1 stays at the table and presents the Teams solution/answers as a host for the visitors

2. The other Teammembers each goes to different Teams

3. The host in each Team presents his/her Teams solution/answers to the guests.

4. The guests thank the host and returns to their original Team.

5. The Teams uses **Circle of knowledge** and shares the most interesting knowledge from the other Teams


**Three for tea is a rapid and efficient way to share knowledge, solutions or ideas with other Teams**
