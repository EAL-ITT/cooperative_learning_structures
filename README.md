![Build Status](https://gitlab.com/EAL-ITT/cooperative_learning_structures/badges/master/pipeline.svg)


# Cooperative learning structures

For use in any course

public website for students:

*  [gitlab pages](https://eal-itt.gitlab.io/cooperative_learning_structures/)

*  [cooperative_learning_Structures.pdf](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf)