---
Structure: 02
Content: Round table
Initials: NISI
---

# Round Table

1. A subject or assignment with multiple answers needs to be processed.
2. Team members takes turns, each writing their answers on a piece of paper.

**Round table ensures that every team member participates and is encouraged to think fast and efficient**